#!/bin/sh

echo "OPTIONS: ${OPTIONS}"
echo "config hosts ${SOURCE} ${TARGET}"

echo "mc alias set msource ${SOURCE} ${SOURCE_ACCESS_KEY} xxxx --api s3v4"
mc alias set msource "${SOURCE}" "${SOURCE_ACCESS_KEY}" "${SOURCE_SECRET_KEY}" --api s3v4

echo "mc alias set mtarget ${TARGET} ${TARGET_ACCESS_KEY} xxxx --api s3v4"
mc alias set mtarget "${TARGET}" "${TARGET_ACCESS_KEY}" "${TARGET_SECRET_KEY}" --api s3v4

echo "Mirror mc mirror --overwrite -a -q --json ${OPTIONS} msource/${SOURCE_BUCKET} mtarget/${TARGET_BUCKET}"
mc_output=$(mc mirror --overwrite -a -q --json ${OPTIONS} msource/${SOURCE_BUCKET} mtarget/${TARGET_BUCKET})
mc_rc=$?
echo "${mc_output}" | tee -a result_tmp.out

exit ${mc_rc}
