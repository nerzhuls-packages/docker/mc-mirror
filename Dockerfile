FROM minio/mc:RELEASE.2021-10-07T04-19-58Z
ENV SOURCE https://SOURCE
ENV TARGET https://TARGET
ENV SOURCE_ACCESS_KEY test
ENV SOURCE_SECRET_KEY test123456
ENV TARGET_ACCESS_KEY test1
ENV TARGET_SECRET_KEY test1234567
ENV SOURCE_BUCKET test
ENV TARGET_BUCKET test1
ENV OPTIONS ""
ENV PRODUCT ""
ENV USER=mc
ENV MC_HOME="/home/mc"
ENV UID 1001
RUN groupadd --system $USER --gid $UID && adduser \
    --uid $UID \
    --home "$MC_HOME" \
    --gid "$USER" \
    "$USER"
RUN chown -R ${USER}:${USER} ${MC_HOME} && chmod -R 750 ${MC_HOME}
USER ${USER}
WORKDIR /home/mc
COPY --chown=${USER}:${USER} entrypoint.sh .
ENTRYPOINT []
CMD ["/bin/sh", "-c" ,"./entrypoint.sh"]

